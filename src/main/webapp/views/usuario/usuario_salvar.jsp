<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Usu�rio">

<jsp:attribute name="body_area">
	<div class="container">
	
			<form:form action="/user/save" method="${empty userForm.id? 'POST' : 'PUT'}" modelAttribute="userForm" >
        												
            <h2 class="form-signin-heading">Cadastro de Usu�rio</h2>
            <hr />
            
			<spring:bind path="id"><form:hidden path="id"/></spring:bind>
			            
            <div class="form-row">
	            <spring:bind path="username">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="username">Login:</form:label><br />
	                    <form:input type="text" path="username" class="form-control" autofocus="true"/>
	               		<form:errors path="username"></form:errors>
	                </div>
	            </spring:bind>
            </div>
            
	        <div class="form-row">    
	            <spring:bind path="password">
	                <div class="form-group ${status.error ? 'has-error' : ''}">
	                	<form:label path ="password">Senha:</form:label><br />
	                    <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
	                    <form:errors path="password"></form:errors>
	                </div>
	            </spring:bind>
            </div>
            
			<div class="form-row">
		        <spring:bind path="passwordConfirm">
	                <div class="form-group ${status.error ? 'has-error' : ''}">
	                    <form:input type="password" path="passwordConfirm" class="form-control"
	                                placeholder="Confirm your password"></form:input>
	                    <form:errors path="passwordConfirm"></form:errors>
	                </div>
	            </spring:bind>
            </div>
   
            <div class="form-row">
	            <spring:bind path="role">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path ="role">Perfil:</form:label><br />
						<div class="form-inline">
							<form:select path="role" class="form-control" style="width:200px">
							   <form:option value="null" label="--- Selecione ---" />
							   <form:option value="ADMIN" label="Admin" />
							   <form:option value="DENTISTA" label="Dentista" />
							   <form:option value="ATENDENTE" label="Atendente" />								   
							</form:select>
						</div>
					</div>
				</spring:bind> 	
            </div>
        <br />
        <br />
			<div class="form-row">
       			<div class="form-group col-md-auto" >
            		<a class="btn btn-lg btn-secondary btn-block" href="/dentista" >Voltar</a>
       			</div>	
				<div class="form-group col-md-2" >
            		<button class="btn btn-lg btn-primary btn-block" type="submit">Salvar</button>
       			</div>	
       		</div>
        </form:form>
	</div>
	
</jsp:attribute>
	
</tag:template>