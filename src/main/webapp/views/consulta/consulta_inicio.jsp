<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Consultas">

<jsp:attribute name="head_area">
	<div class="container-fluid">
		<div >
			<h1>Consultas</h1>
			<hr>
		</div>
		<div class="row justify-content-end">
		    <h4 style="margin-right: 15px;"><a href="/consulta/form">Cadastrar Nova</a></h4>
		</div>
	</div>
</jsp:attribute>

<jsp:attribute name="body_area">	 	
	<div class="container-fluid" style="margin-top: 10px;">
		
		<c:if test="${mensagem != null}">
			<div class="alert alert-info" role="alert">
				<span>${mensagem}</span>
			</div>
		</c:if>
	
		<table class="table table-striped">
			<tr>
				<th>Cod</th>
				<th>Data</th>
				<th>Hor�rio</th>
				<th>Hor�rio</th>
				<th>Paciente</th>
				<th colspan=2 style="text-align:center">A��es</th>
			</tr>
			<c:forEach var="consulta" items="${consultas}">
				<tr>
					<td>${consulta.id}</td>
					<td>${consulta.horario.data}</td>
					<td>
						${consulta.horario.horaInicial} <br />
						${consulta.horario.horaFinal}					
					</td>
					<td>${consulta.horario.dentista.nome}</td>
					<td>${consulta.paciente.nome}</td>
					<td>
						<form:form action="consulta/form/${consulta.id}" method="GET" modelAttribute="id">
							<button class="btn btn-lg btn-primary btn-block" type="submit">Editar</button>
						</form:form>
					</td>
					<td>
						<form:form action="consulta/delete/${consulta.id}" method="GET" modelAttribute="id">
							<button class="btn btn-lg btn-danger btn-block" type="submit">Excluir</button>
						</form:form>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<c:if test="${empty consultas}">
			<br /> Nenhum registro encontrado.
		</c:if>
	</div>
</jsp:attribute>
	
</tag:template>
