<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Consultas">

<jsp:attribute name="body_area">
	<div class="container">
	
			<form:form action="/consulta/save" method="${empty dentistaForm.id? 'POST' : 'PUT'}" modelAttribute="consultaForm" >
        												
            <h2 class="form-signin-heading">Cadastro de Consulta</h2>
            <hr />
            
			<spring:bind path="id"><form:hidden path="id"/></spring:bind>
			            
            <div class="form-row">
	            <spring:bind path="paciente">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path ="paciente">Paciente:</form:label><br />
						<div class="form-inline">
							<form:select path="paciente" class="form-control" style="width:320px">
							   <form:option value="null" label="--- Selecione ---" />
							   <form:options items="${pacientes}" itemValue="id" itemLabel="nome"/>
							</form:select>
		    				<h4 style="margin-right: 15px; margin-left: 20px;"><a href="/paciente/form" class="btn btn-info">Novo Paciente</a></h4>
						</div>
					</div>
				</spring:bind> 	
            </div>

			<div class="form-row">
				<spring:bind path="horario.dentista">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path ="horario.dentista">Dentista:</form:label><br />
						<form:select path="horario.dentista" class="form-control" style="width:320px">
						   <form:option value="null" label="--- Selecione ---" />
						   <form:options items="${dentistas}" itemValue="id" itemLabel="nome"/>
						</form:select>
					</div>
				</spring:bind>
			</div>
			
			
        <br />
        <br />
			<div class="form-row">
       			<div class="form-group col-md-auto" >
            		<a class="btn btn-lg btn-secondary btn-block" href="/consulta" >Voltar</a>
       			</div>	
				<div class="form-group col-md-2" >
            		<button class="btn btn-lg btn-primary btn-block" type="submit">Salvar</button>
       			</div>	
       		</div>
        </form:form>
	</div>
	
</jsp:attribute>
	
</tag:template>