<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Paciente">

<jsp:attribute name="head_area">
	<div class="container-fluid">
		<div >
			<h1>Pacientes</h1>
			<hr>
		</div>
		<div class="row justify-content-end">
		    <h4 style="margin-right: 15px;"><a href="/paciente/form">Cadastrar Novo</a></h4>
		</div>
	</div>
</jsp:attribute>

<jsp:attribute name="body_area">	 	
	<div class="container-fluid" style="margin-top: 10px;">
		
		<c:if test="${mensagem != null}">
			<div class="alert alert-info" role="alert">
				<span>${mensagem}</span>
			</div>
		</c:if>
	
		<table class="table table-striped">
			<tr>
				<th>Nome</th>
				<th>Telefones</th>
				<th>Localidade</th>
				<th>E-mail</th>
				<th>�ltima Consulta</th>
				<th colspan=2 style="text-align:center">A��es</th>
			</tr>
			<c:forEach var="paciente" items="${pacientes}">
				<tr>
					<td>${paciente.nome}</td>
					<td>
						${paciente.telefone1} <br />
						${paciente.telefone2}					
					</td>
					<td>
						${paciente.endereco.bairro} <br />
						${paciente.endereco.cidade} / ${paciente.endereco.uf}
					</td>
					<td>${paciente.email}</td>
					<td>???</td>
					<td>
						<form:form action="paciente/form/${paciente.id}" method="GET" modelAttribute="id">
							<button class="btn btn-lg btn-primary btn-block" type="submit">Editar</button>
						</form:form>
					</td>
					<td>
						<form:form action="paciente/delete/${paciente.id}" method="GET" modelAttribute="id">
							<button class="btn btn-lg btn-danger btn-block" type="submit">Excluir</button>
						</form:form>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<c:if test="${empty pacientes}">
			<br /> Nenhum registro encontrado.
		</c:if>
	</div>
</jsp:attribute>
	
</tag:template>
