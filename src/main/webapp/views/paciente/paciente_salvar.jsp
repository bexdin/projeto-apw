<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Paciente">

<jsp:attribute name="body_area">
	<div class="container">
	
			<form:form action="/paciente/save" method="${empty pacienteForm.id? 'POST' : 'PUT'}" modelAttribute="pacienteForm" >
        												
            <h2 class="form-signin-heading">Cadastro de Paciente</h2>
            <hr />
            
			<spring:bind path="id"><form:hidden path="id"/></spring:bind>
			            
            <div class="form-row">
	            <spring:bind path="nome">
	                <div class="form-group col-md-10 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="nome">Nome:</form:label><br />
	                    <form:input type="text" path="nome" class="form-control"/>
	                    <form:errors path="nome"></form:errors>
	                </div>
	            </spring:bind>
            </div>

			<div class="form-row">
	            <spring:bind path="cpf">
	                <div class="form-group col-md-4 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="cpf">CPF:</form:label><br />
	                    <form:input type="text" path="cpf" class="form-control" 
	                    				placeholder="000.000.000-00" onkeypress="$(this).mask('000.000.000-00');"/>
	                    <form:errors path="cpf"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="sexo">
	                <div class="form-group col-md-3 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="sexo">Sexo:</form:label><br />
	                	 &nbsp &nbsp &nbsp
	                	 <form:radiobutton path="sexo" value="F" label="Feminino"/>
	                	 &nbsp &nbsp &nbsp
	                	 <form:radiobutton path="sexo" value="M" label="Masculino"/>
	                	<form:errors path="sexo"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="dataNascimento">
	                <div class="form-group col-md-3 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="dataNascimento">Data de Nascimento:</form:label><br />
	                    <form:input type="date" path="dataNascimento" class="form-control"/>
	                    <form:errors path="dataNascimento"></form:errors>
	                </div>
	            </spring:bind>
            </div>
            
            <div class="form-row">
            	<spring:bind path="endereco.cep">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.cep">CEP:</form:label><br />
	                    <form:input type="text" path="endereco.cep" class="form-control"
	                    				placeholder="00.000.000" onkeypress="$(this).mask('(00) 0000-00009');"/>
	                    <form:errors path="endereco.cep"></form:errors>
	                </div>
	            </spring:bind>
	            <spring:bind path="endereco.logradouro">
	                <div class="form-group col-md-8 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.logradouro">Logradouro:</form:label><br />
	                    <form:input type="text" path="endereco.logradouro" class="form-control"/>
	                    <form:errors path="endereco.logradouro"></form:errors>
	                </div>
	            </spring:bind>
	        </div>
	            
            <div class="form-row">
	            <spring:bind path="endereco.numero">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.numero">N�mero:</form:label><br />
	                    <form:input type="text" path="endereco.numero" class="form-control"/>
	                    <form:errors path="endereco.numero"></form:errors>
	                </div>
	            </spring:bind>
	            <spring:bind path="endereco.complemento">
	                <div class="form-group col-md-4 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.complemento">Complemento:</form:label><br />
	                    <form:input type="text" path="endereco.complemento" class="form-control"/>
	                    <form:errors path="endereco.complemento"></form:errors>
	                </div>
	            </spring:bind>
	            <spring:bind path="endereco.bairro">
	                <div class="form-group col-md-4 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.bairro">Bairro:</form:label><br />
	                    <form:input type="text" path="endereco.bairro" class="form-control"/>
	                    <form:errors path="endereco.bairro"></form:errors>
	                </div>
	            </spring:bind>            
            </div>
            
             <div class="form-row">
	            <spring:bind path="endereco.cidade">
	                <div class="form-group col-md-5 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.cidade">Cidade:</form:label><br />
	                    <form:input type="text" path="endereco.cidade" class="form-control"/>
	                    <form:errors path="endereco.cidade"></form:errors>
	                </div>
	            </spring:bind>
	            <spring:bind path="endereco.estado">
	                <div class="form-group col-md-4 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.estado">Estado:</form:label><br />
	                    <form:input type="text" path="endereco.estado" class="form-control"/>
	                    <form:errors path="endereco.estado"></form:errors>
	                </div>
	            </spring:bind>
	            <spring:bind path="endereco.uf">
	                <div class="form-group col-md-1 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="endereco.uf">UF:</form:label><br />
	                    <form:input type="text" path="endereco.uf" class="form-control"/>
	                    <form:errors path="endereco.uf"></form:errors>
	                </div>
	            </spring:bind>            
            </div>
            
            <div class="form-row">
	            <spring:bind path="telefone1">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="telefone1">Telefone:</form:label><br />
	                    <form:input type="tel" path="telefone1" class="form-control" 
	                    				placeholder="(00) 00000-0000" onkeypress="$(this).mask('(00) 0000-00009');"/>
	                    <form:errors path="telefone1"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="telefone2">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="telefone2">Telefone:</form:label><br />
	                    <form:input type="tel" path="telefone2" class="form-control" 
	                    				placeholder="(00) 00000-0000" onkeypress="$(this).mask('(00) 0000-00009');"/>
	                    <form:errors path="telefone2"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="email">
	                <div class="form-group col-md-6 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="email">E-mail:</form:label><br />
	                    <form:input type="email" path="email" class="form-control"/>
	                    <form:errors path="email"></form:errors>
	                </div>
	            </spring:bind>
            </div>
        <br />
        <br />
			<div class="form-row">
       			<div class="form-group col-md-auto" >
            		<a class="btn btn-lg btn-secondary btn-block" href="/paciente" >Voltar</a>
       			</div>	
				<div class="form-group col-md-2" >
            		<button class="btn btn-lg btn-primary btn-block" type="submit">Salvar</button>
       			</div>	
       		</div>
        </form:form>
	</div>
	
</jsp:attribute>
	
</tag:template>