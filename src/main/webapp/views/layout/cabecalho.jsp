<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
		<a class="navbar-brand mb-0 h1" href="#">SisOdonto</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item active"><a class="nav-link" href="/dentista">Home<span
					class="sr-only">(current)</span></a></li>
			<li class="nav-item"><a class="nav-link" href="#">Cadastros</a></li>
			<li class="nav-item"><a class="nav-link disabled" href="#"
				tabindex="-1" aria-disabled="true">Agenda</a></li>
		</ul>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<div class="form-inline" style="margin-right:20px">
				<div class="btn-group">
				  <button type="button" class="btn btn-light" style="padding-left:20px; padding-right:20px;"><h4>${pageContext.request.userPrincipal.name}</h4></button>
				  <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <span class="sr-only"></span>
				  </button>
				  <div class="dropdown-menu">
				    <div class="d-flex justify-content-center">
				        <form id="logoutForm" method="POST" action="${contextPath}/logout" class="form-inline my-2 my-lg-0">
				            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				            <button class="btn btn-primary " type="submit">Sair</button>
				        </form>
			        </div>
				  </div>
				</div>
			</div>
	    </c:if>
	</div>
</nav>