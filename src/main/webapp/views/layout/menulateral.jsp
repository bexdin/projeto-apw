<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>HOME</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body style="margin-top: 70px">

	<div class="navbar-expand-lg">
		<nav class="nav nav-pills flex-column">
			<a class="nav-link active" href="/consulta">Consultas</a> 
			<a class="nav-link"	href="/paciente">Pacientes</a> 
			<a class="nav-link" href="/dentista">Dentistas</a>
			<a class="nav-link" href="/horario">Hor�rios</a>
		</nav>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>