<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Dentista">

<jsp:attribute name="body_area">
	<div class="container">
	
			<form:form action="/dentista/save" method="${empty dentistaForm.id? 'POST' : 'PUT'}" modelAttribute="dentistaForm" >
        												
            <h2 class="form-signin-heading">Cadastro de Dentista</h2>
            <hr />
            
			<spring:bind path="id"><form:hidden path="id"/></spring:bind>
			            
            <div class="form-row">
	            <spring:bind path="cro">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="cro">CRO:</form:label><br />
	                    <form:input type="text" path="cro" class="form-control" autofocus="true"/>
	               		<form:errors path="cro"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="nome">
	                <div class="form-group col-md-8 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="nome">Nome:</form:label><br />
	                    <form:input type="text" path="nome" class="form-control"/>
	                    <form:errors path="nome"></form:errors>
	                </div>
	            </spring:bind>
            </div>

			<div class="form-row">
	            <spring:bind path="cpf">
	                <div class="form-group col-md-4 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="cpf">CPF:</form:label><br />
	                    <form:input type="text" path="cpf" class="form-control" 
	                    				placeholder="000.000.000-00" onkeypress="$(this).mask('000.000.000-00');"/>
	                    <form:errors path="cpf"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="sexo">
	                <div class="form-group col-md-3 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="sexo">Sexo:</form:label><br />	                	
	                	<form:radiobutton path="sexo" value="F" label="Feminino" class="form-check-inline" style="margin-left:15px"/>
	                	<form:radiobutton path="sexo" value="M" label="Masculino" class="form-check-inline" style="margin-left:15px"/>
	                	<form:errors path="sexo"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="dataNascimento">
	                <div class="form-group col-md-3 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="dataNascimento">Data de Nascimento:</form:label><br />
	                    <form:input type="date" path="dataNascimento" class="form-control"/>
	                    <form:errors path="dataNascimento"></form:errors>
	                </div>
	            </spring:bind>
            </div>
            
            <div class="form-row">
	            <spring:bind path="telefone1">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="telefone1">Telefone:</form:label><br />
	                    <form:input type="tel" path="telefone1" class="form-control" 
	                    				placeholder="(00) 00000-0000" onkeypress="$(this).mask('(00) 0000-00009');"/>
	                    <form:errors path="telefone1"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="telefone2">
	                <div class="form-group col-md-2 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="telefone2">Telefone:</form:label><br />
	                    <form:input type="tel" path="telefone2" class="form-control" 
	                    				placeholder="(00) 00000-0000" onkeypress="$(this).mask('(00) 0000-00009');"/>
	                    <form:errors path="telefone2"></form:errors>
	                </div>
	            </spring:bind>
	            
	            <spring:bind path="email">
	                <div class="form-group col-md-6 ${status.error ? 'has-error' : ''}">
	                	<form:label path ="email">E-mail:</form:label><br />
	                    <form:input type="email" path="email" class="form-control"/>
	                    <form:errors path="email"></form:errors>
	                </div>
	            </spring:bind>
            </div>
        <br />
        <br />
			<div class="form-row">
       			<div class="form-group col-md-auto" >
            		<a class="btn btn-lg btn-secondary btn-block" href="/dentista" >Voltar</a>
       			</div>	
				<div class="form-group col-md-2" >
            		<button class="btn btn-lg btn-primary btn-block" type="submit">Salvar</button>
       			</div>	
       		</div>
        </form:form>
	</div>
	
</jsp:attribute>
	
</tag:template>