<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Hor�rios">

<jsp:attribute name="body_area">
	<div class="container">
	
			<form:form action="/horario/save" method="POST" modelAttribute="horarioForm" >
        												
            <h2 class="form-signin-heading">Cadastro de Hor�rios</h2>
            <hr />
            
			<spring:bind path="id"><form:hidden path="id"/></spring:bind>
			            
            <div class="form-row">
		            <spring:bind path="data">
		                <div class="form-group col-md ${status.error ? 'has-error' : ''}">
		                	<form:label path ="data">Data:</form:label><br />
		                    <form:input type="date" path="data" class="form-control"/>
		                    <form:errors path="data"></form:errors>
		                </div>
		            </spring:bind>
		            
		            <spring:bind path="horaInicial">
		                <div class="form-group col-md ${status.error ? 'has-error' : ''}">
		                	<form:label path ="horaInicial">In�cio</form:label><br />
		                    <form:input type="time" path="horaInicial" class="form-control"/>
		                    <form:errors path="horaInicial"></form:errors>
		                </div>
		            </spring:bind>
		            
		            <spring:bind path="horaFinal">
		                <div class="form-group col-md ${status.error ? 'has-error' : ''}">
		                	<form:label path ="horaFinal">T�rmino:</form:label><br />
		                    <form:input type="time" path="horaFinal" class="form-control"/>
		                    <form:errors path="horaFinal"></form:errors>
		                </div>
		            </spring:bind>
	            </div>

				<br />
				<div class="form-row">
					<spring:bind path="dentista">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<form:label path ="dentista">Dentista:</form:label><br />
							<form:select path="dentista" class="btn btn-outline dropdown-toggle" style="width:320px">
							   <form:option value="null" label="--- Selecione ---" />
							   <form:options items="${dentistas}" itemValue="id" itemLabel="nome"/>
							</form:select>
						</div>
					</spring:bind>
				</div>
        <br />
        <br />
			<div class="form-row">
       			<div class="form-group col-md-auto" >
            		<a class="btn btn-lg btn-secondary btn-block" href="/horario" >Voltar</a>
       			</div>	
				<div class="form-group col-md-2" >
            		<button class="btn btn-lg btn-primary btn-block" type="submit">Salvar</button>
       			</div>	
       		</div>
        </form:form>
	</div>
	
</jsp:attribute>
	
</tag:template>