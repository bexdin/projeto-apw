<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tag:template title="SisOdonto - Dentista">

<jsp:attribute name="head_area">
	<div class="container-fluid">
		<div >
			<h1>Hor�rios</h1>
			<hr>
		</div>
		<div class="row justify-content-end">
		    <h4 style="margin-right: 15px;"><a href="/horario/form">Cadastrar Novo</a></h4>
		</div>
	</div>
</jsp:attribute>

<jsp:attribute name="body_area">	 	
	<div class="container-fluid" style="margin-top: 10px;">
		
		<c:if test="${mensagem != null}">
			<div class="alert alert-info" role="alert">
				<span>${mensagem}</span>
			</div>
		</c:if>
	
		<table class="table table-striped">
			<tr>
				<th>Data</th>
				<th>Hor�rio</th>
				<th>Dentista</th>
				<th colspan=2 style="text-align:center">A��es</th>
			</tr>
			<c:forEach var="horario" items="${horarios}">
				<tr>
					<td>${horario.data}</td>
					<td>
						${horario.horaInicial} <br />
						${horario.horaFinal}					
					</td>
					<td>${horario.dentista.nome}</td>
					<td>
						<form:form action="horario/delete/${horario.id}" method="GET" modelAttribute="id">
							<button class="btn btn-lg btn-danger btn-block" type="submit">Excluir</button>
						</form:form>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<c:if test="${empty horarios}">
			<br /> Nenhum registro encontrado.
		</c:if>
	</div>
		
</jsp:attribute>
	
</tag:template>
