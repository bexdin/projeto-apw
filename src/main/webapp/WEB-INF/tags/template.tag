<%@tag import="org.springframework.context.annotation.Import"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!-- Dica: https://mobiarch.wordpress.com/2013/01/04/page-templating-using-jsp-custom-tag/ -->

<!DOCTYPE html>
<%@tag description="Template" pageEncoding="UTF-8"%>

<%@attribute name="title"%>
<%@attribute name="head_area" fragment="true" %>
<%@attribute name="body_area" fragment="true" %>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <title>${title}</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    <link href="${contextPath}/resources/fontAwesome/css/all.css" rel="stylesheet">
    
</head>

<body>
	<div class="container-fluid">
	
		<div class="row">
			<c:import url="/views/layout/cabecalho.jsp"/>
		</div>
		 
		<div class="row">	
			<div class="col-2">
				<c:import url="/views/layout/menulateral.jsp"/>
			</div>
			
			<div class="col-9" style="margin-left: 10px;">
				<div>
					<!-- conteúdo dinâmico -->
					<div>
						<jsp:invoke fragment="head_area"/>
					</div>
					<div>
						<jsp:invoke fragment="body_area"/>
					</div>
				</div>
			</div>
		</div>
		
		<footer class="row"> 
			<div class="fixed-bottom">
				<span>@Copyrigth 2019</span>
			</div>
		</footer>
		
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
  	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>

</html>