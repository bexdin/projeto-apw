package br.com.sisodonto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sisodonto.model.Dentista;

@Repository
public interface DentistaRepository extends JpaRepository<Dentista, Long>{
	
	public Dentista findByNome(String nome);
	public Dentista findByCro(String cro);
	public List<Dentista> findBySexo(String sexo);
	
}

 /*
 * APRENDENDO SPRING: JPAREPOSITORY
 * FONTE: https://edermfl.wordpress.com/2017/04/27/aprendendo-spring-5-jparepository/
 * 
 * Interface JPARepository
 * FONTE: https://blog.algaworks.com/spring-data-jpa/
 */