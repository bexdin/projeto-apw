package br.com.sisodonto.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sisodonto.model.Paciente;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long>{
	
	public Paciente findByNome(String nome);
	public Paciente findByCpf(String cpf);
	public List<Paciente> findByDataNascimento(Date data);
	public List<Paciente> findBySexo(String sexo);
	
	// a keyword 'Containing' é o mesmo que WHERE paciente.nome LIKE %nome% e 'IgnoreCase' serve para não considerar se letras maiúsculas ou minúsculas
	public List<Paciente> findByNomeContainingIgnoreCase(String nome);	
}

/*
 * APRENDENDO SPRING: JPAREPOSITORY
 * FONTE: https://edermfl.wordpress.com/2017/04/27/aprendendo-spring-5-jparepository/
 * 
 * Interface JPARepository
 * FONTE: https://blog.algaworks.com/spring-data-jpa/
 */