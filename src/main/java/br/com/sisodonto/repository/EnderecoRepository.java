package br.com.sisodonto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sisodonto.model.Endereco;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long>{
	
	public Endereco findByCep(String cep);
	public List<Endereco> findByBairro(String bairro);
	public List<Endereco> findByCidade(String cidade);
	public List<Endereco> findByEstado(String estado);

}

/*
 * APRENDENDO SPRING: JPAREPOSITORY
 * FONTE: https://edermfl.wordpress.com/2017/04/27/aprendendo-spring-5-jparepository/
 * 
 * Interface JPARepository
 * FONTE: https://blog.algaworks.com/spring-data-jpa/
 */
