package br.com.sisodonto.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.sisodonto.model.Consulta;
import br.com.sisodonto.model.Paciente;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, Long>{
	
	public List<Consulta> findByPaciente(Paciente paciente);
	
	/**
	 * Usando JPQL
	 * @param data
	 * @return lista de consultas com a data informada
	 */
	@Query("SELECT c FROM Consulta c JOIN c.horario h WHERE h.data= ?1")
	public List<Consulta> findByData(Date data);
	
	/**
	 * Usando JPQL com "parâmetros nomeados"
	 * @param idDentista
	 * @return lista de consultas do dentista informado
	 */
	@Query("SELECT c FROM Consulta c JOIN c.horario h WHERE h.dentista= :idDentista")
	public List<Consulta> findByDentista(@Param("idDentista") Long idDentista);
	
}

/*
 * APRENDENDO SPRING: JPAREPOSITORY
 * FONTE: https://edermfl.wordpress.com/2017/04/27/aprendendo-spring-5-jparepository/
 * 
 * Interface JPARepository
 * FONTE: https://blog.algaworks.com/spring-data-jpa/
 * 
 * Example 61. Using named parameters
 * FONTE: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query
 */
