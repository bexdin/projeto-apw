package br.com.sisodonto.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.model.Horario;

@Repository
public interface HorarioRepository extends JpaRepository<Horario, Long>{
	
	public List<Horario> findByData(Date data);
	public List<Horario> findByDentista(Dentista dentista);

}

/*
 * APRENDENDO SPRING: JPAREPOSITORY
 * FONTE: https://edermfl.wordpress.com/2017/04/27/aprendendo-spring-5-jparepository/
 * 
 * Interface JPARepository
 * FONTE: https://blog.algaworks.com/spring-data-jpa/
 */
