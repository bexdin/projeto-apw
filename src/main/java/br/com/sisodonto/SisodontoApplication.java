package br.com.sisodonto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SisodontoApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SisodontoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SisodontoApplication.class, args);
	}

}


/*
 * O Boot sabe que se trata da classe de entrada porque ela possui o método main() e pela invocação de SpringApplication.run().
 * Esse método aceita uma classe que seja fonte de configuração (contenha a anotação @Configuration) e parâmetros repassados 
 * da linha de comando.
 * Mas ela não está anotada com @Configuration nem está configurando nada! ” – pode-se argumentar. 
 * Acontece que ela é e está configurando tudo em nossa aplicação. 
 * Pois, A anotação @SpringBootApplication é uma anotação de conveniência que contém as seguintes anotações do Spring:
 * @Configuration, @EnableAutoConfiguration e @ComponentScan.
 * Essas duas últimas, basicamente, dizem ao inicializador do Spring: “Busque e instancie todo bean anotado deste pacote para frente”.
 * Com “bean anotado” estamos nos referindo a classes anotadas com @Configuration e métodos que retornam @Bean, e classes anotadas 
 * com @Service, @Component, @Controller e @RestController, por exemplo.
 * Logo, a classe Application não configura nada, mas dispara a busca por beans a partir de seu pacote.
 * Esse processo de autoconfiguração e busca de componentes, como se sabe, não é especificamente do Boot, e sim do próprio Spring Framework.
 * 
 * >>> FONTE: https://www.devmedia.com.br/primeiros-passos-com-o-spring-boot/33654
 *
*/