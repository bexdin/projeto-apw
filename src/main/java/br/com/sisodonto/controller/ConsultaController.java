package br.com.sisodonto.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisodonto.model.Consulta;
import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.model.Paciente;
import br.com.sisodonto.service.ConsultaService;
import br.com.sisodonto.service.DentistaService;
import br.com.sisodonto.service.PacienteService;

@Controller
public class ConsultaController {
	
	@Autowired
	private ConsultaService consultaService;
	
	@Autowired
	private DentistaService dentistaService;
	
	@Autowired
	private PacienteService pacienteService;
	
	
	@GetMapping("/consulta")	// LISTA AS CONSULTAS (PÁGINA INICIAL DE CONSULTA)
	public String listConsultas(Model model) {
		
		List<Consulta> consultas = consultaService.findAll();
		model.addAttribute("consultas", consultas);

		return "views/consulta/consulta_inicio";
	}
	
	@GetMapping("/consulta/form")	// ABRE A PÁGINA DE CADASTRO DE NOVA CONSULTA
	public String prepareConsultaForm(Model model) {
		
		Consulta consulta= new Consulta();
		model.addAttribute("consultaForm", consulta);
		List<Dentista> dentistas = dentistaService.findAll();
		List<Paciente> pacientes = pacienteService.findAll();
		model.addAttribute("dentistas", dentistas);
		model.addAttribute("pacientes", pacientes);
				
		return "views/consulta/consulta_salvar";
	}
	
	@GetMapping("/consulta/form/{id}")	// ABRE A PÁGINA DE CADASTRO PARA ATUALIZAR UMA CONSULTA
	public String prepareUpdateConsultaForm(@PathVariable ("id") Long id, Model model) {
		
		Consulta consulta = consultaService.findById(id);
		model.addAttribute("dentistaForm", consulta);
		List<Dentista> dentistas = dentistaService.findAll();
		List<Paciente> pacientes = pacienteService.findAll();
		model.addAttribute("dentistas", dentistas);
		model.addAttribute("pacientes", pacientes);
				
		return "views/consulta/consulta_salvar";
	}
	
	@PostMapping("/consulta/save")	// SALVA A CONSULTA E RETORNA À LISTA DE CONSULTAS
	public String save(@Valid @ModelAttribute("consultaForm") Consulta consulta, BindingResult bindingResult, RedirectAttributes attr) {
		
		 if (bindingResult.hasErrors()) {
	            return "views/consulta/consulta_salvar";
	     }
		 consultaService.save(consulta, consulta.getHorario().getId());
		 attr.addFlashAttribute("mensagem", "Consulta cadastrada com sucesso.");
		 
		 return "redirect:/consulta";
	}
	
	@PutMapping("/consulta/save") 	// APÓS EDIÇÃO SALVA A CONSULTA E RETORNA À LISTA DE CONSULTAS
	public String update(@Valid @ModelAttribute("consultaForm") Consulta consulta, BindingResult bindingResult, RedirectAttributes attr) {
		
		if (bindingResult.hasErrors()) {
            return "views/consulta/consulta_salvar";
	     }
		 consultaService.save(consulta, consulta.getHorario().getId());
		 attr.addFlashAttribute("mensagem", "Consulta atualizada com sucesso.");
		 
		 return "redirect:/consulta";
	}
	
	@GetMapping("/consulta/delete/{id}")  // EXCLUI A CONSULTA E RETORNA À LISTA DE CONSULTAS
	public String delete(@PathVariable Long id, RedirectAttributes attr) {
		
		consultaService.delete(id);
		attr.addFlashAttribute("mensagem", "Consulta excluída com sucesso.");	

		return "redirect:/dentista";
	}
	
	//FALTA FAZER O ARQUIVO JAVASCRIPT E AJUSTAR O JSP PARA ATRAVÉS DESSE CAMINHO GERAR O AUTOCOMPLETE DE PACIENTE
	@GetMapping("/consulta/{paciente}")
	@ResponseBody
	public List<Paciente> loadPaciente(@PathVariable("paciente")Paciente paciente){
		return pacienteService.findByNomeContainingIgnoreCase(paciente.getNome());		
	}
	
	
}
