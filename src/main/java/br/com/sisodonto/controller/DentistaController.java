package br.com.sisodonto.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.service.DentistaService;

@Controller
public class DentistaController {
	
	@Autowired
	private DentistaService dentistaService;
	

	
	@GetMapping("/dentista")	// LISTA OS DENTISTAS (PÁGINA INICIAL DE DENTISTA)
	public String listDentistas(Model model) {
		
		List<Dentista> dentistas = dentistaService.findAll();
		model.addAttribute("dentistas", dentistas);

		return "views/dentista/dentista_inicio";
	}
	
	@GetMapping("/dentista/form")	// ABRE A PÁGINA DE CADASTRO DE NOVO DENTISTA
	public String prepareDentistaForm(Model model) {
		
		Dentista dentista = new Dentista();
		model.addAttribute("dentistaForm", dentista);
				
		return "views/dentista/dentista_salvar";
	}
	
	@GetMapping("/dentista/form/{id}")	// ABRE A PÁGINA DE CADASTRO PARA ATUALIZAR UM DENTISTA
	public String prepareUpdateDentistaForm(@PathVariable ("id") Long id, Model model) {
		
		// FALTA COLOCAR... IF dentista != null ... else msg de não encontrado
		Dentista dentista = dentistaService.findById(id);
		model.addAttribute("dentistaForm", dentista);
				
		return "views/dentista/dentista_salvar";
	}

	@PostMapping("/dentista/save")	// SALVA O DENTISTA E RETORNA À LISTA DE DENTISTAS
	public String save(@Valid @ModelAttribute("dentistaForm") Dentista dentista, BindingResult bindingResult, RedirectAttributes attr) {
		
		 if (bindingResult.hasErrors()) {
	            return "views/dentista/dentista_salvar";
	     }
		 dentistaService.save(dentista);
		 attr.addFlashAttribute("mensagem", "Dentista cadastrado com sucesso.");
		 
		 return "redirect:/dentista";
	}

	@PutMapping("/dentista/save") 	// APÓS EDIÇÃO SALVA O DENTISTA E RETORNA À LISTA DE DENTISTAS
	public String update(@Valid @ModelAttribute("dentistaForm") Dentista dentista, BindingResult bindingResult, RedirectAttributes attr) {
		
		if (bindingResult.hasErrors()) {
            return "views/dentista/dentista_salvar";
	     }
		 dentistaService.save(dentista);
		 attr.addFlashAttribute("mensagem", "Dentista atualizado com sucesso.");
		 
		 return "redirect:/dentista";
	}

	@GetMapping("/dentista/delete/{id}")  // EXCLUI O DENTISTA E RETORNA À LISTA DE DENTISTAS
	public String delete(@PathVariable Long id, RedirectAttributes attr) {
		
		dentistaService.delete(id);
		attr.addFlashAttribute("mensagem", "Dentista excluído com sucesso.");	

		return "redirect:/dentista";
	}
	
}
