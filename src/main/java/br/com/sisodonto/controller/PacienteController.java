package br.com.sisodonto.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisodonto.model.Paciente;
import br.com.sisodonto.service.PacienteService;

@Controller
public class PacienteController {

	@Autowired
	private PacienteService pacienteService;
	
	
	@GetMapping("/paciente")	// LISTA OS PACIENTES (PÁGINA INICIAL DE PACIENTE)
	public String listPacientes(Model model) {
		
		List<Paciente> pacientes = pacienteService.findAll();
		model.addAttribute("pacientes", pacientes);
		
		return "views/paciente/paciente_inicio";
	}
	
	@GetMapping("/paciente/form")	// ABRE A PÁGINA DE CADASTRO DE NOVO PACIENTE
	public String preparePacienteForm(Model model) {
		
		Paciente paciente = new Paciente();
		model.addAttribute("pacienteForm", paciente);
		
		return "views/paciente/paciente_salvar";
	}
	
	@GetMapping("/paciente/form/{id}")	// ABRE A PÁGINA DE CADASTRO PARA ATUALIZAR UM PACIENTE
	public String prepareUpdatePacienteForm(@PathVariable ("id") Long id, Model model) {
	
		// FALTA COLOCAR... IF paciente != null ... else msg de não encontrado
		Paciente paciente = pacienteService.findById(id);
		model.addAttribute("paciente", paciente);
		
		return "views/paciente/paciente_salvar";
	}
	
	@PostMapping("/paciente/save")	// SALVA O PACIENTE E RETORNA À LISTA DE PACIENTES
	public String save(@Valid @ModelAttribute("pacienteForm") Paciente paciente, BindingResult bindingResult, RedirectAttributes attr) {
		
		 if (bindingResult.hasErrors()) {
	            return "views/paciente/paciente_salvar";
	     }
		 pacienteService.save(paciente);
		 attr.addFlashAttribute("mensagem", "Paciente cadastrado com sucesso.");
		 
		 return "redirect:/paciente";
	}
	
	@GetMapping("/paciente/delete/{id}")  // EXCLUI O PACIENTE E RETORNA À LISTA DE PACIENTES
	public String delete(@PathVariable Long id, RedirectAttributes attr) {
		
		pacienteService.delete(id);
		attr.addFlashAttribute("mensagem", "Paciente excluído com sucesso.");	

		return "redirect:/paciente";
	}
		
}
