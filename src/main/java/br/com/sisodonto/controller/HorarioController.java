package br.com.sisodonto.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.model.Horario;
import br.com.sisodonto.service.DentistaService;
import br.com.sisodonto.service.HorarioService;

@Controller
public class HorarioController {
	
	@Autowired
	private HorarioService horarioService;
	
	@Autowired
	private DentistaService dentistaService;
	
	/** 
	 * Identifica se o intervalo deve ser cadastrado como um único horário ou como uma jornada de trabalho, 
	 * isto é, caso 'true' o intervalo passado será dividido em vários horários de uma hora.
	 */
	@Transient
	private boolean isJornada;
	
	public boolean isJornada() {
		return isJornada;
	}

	public void setJornada(boolean isJornada) {
		this.isJornada = isJornada;
	}	
	
	@GetMapping("/horario")		//LISTA OS HORÁRIOS (PÁGINA INICIAL DE HORÁRIOS)
	public String listHorarios(Model model) {
		
		List<Horario> horarios = horarioService.findAll();
		model.addAttribute("horarios", horarios);
		
		return "views/horario/horario_inicio";
	}
	
	@GetMapping("/horario/form")	// ABRE A PÁGINA DE CADASTRO DE NOVO HORÁRIO
	public String prepareHorarioForm(Model model) {
		
		Horario horario = new Horario();
		model.addAttribute("horarioForm", horario);
		List<Dentista> dentistas = dentistaService.findAll();
		model.addAttribute("dentistas", dentistas);
				
		return "views/horario/horario_salvar";
	}
	
	@PostMapping("/horario/save")	// SALVA O HORÁRIO E RETORNA À LISTA DE HORÁRIOS
	public String save(@Valid @ModelAttribute("horarioForm") Horario horario, BindingResult bindingResult,RedirectAttributes attr) {
		
		 if (bindingResult.hasErrors()) {
	            return "views/horario/horario_salvar";
	     }
		 horarioService.save(horario, horario.getDentista().getId());
		 attr.addFlashAttribute("mensagem", "Horário(s) cadastrado(s) com sucesso.");
		 
		 return "redirect:/horario";
	}
	
	@GetMapping("/horario/delete/{id}")  // EXCLUI O HORÁRIO E RETORNA À LISTA DE DENTISTAS
	public String delete(@PathVariable Long id, RedirectAttributes attr) {
		
		horarioService.delete(id);
		attr.addFlashAttribute("mensagem", "Horário excluído com sucesso.");	

		return "redirect:/horario";
	}

	
	
}
