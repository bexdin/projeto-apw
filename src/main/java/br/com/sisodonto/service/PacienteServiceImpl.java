package br.com.sisodonto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sisodonto.model.Paciente;
import br.com.sisodonto.repository.PacienteRepository;

@Service
@Transactional
public class PacienteServiceImpl implements PacienteService {

	@Autowired
	private PacienteRepository pacienteRepository;

	@Override
	public Paciente save(Paciente paciente) {
		return pacienteRepository.save(paciente);
	}

	@Override
	@Transactional(readOnly=true)
	public Paciente findById(Long id) {
		return pacienteRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public Paciente findByNome(String nome) {
		return pacienteRepository.findByNome(nome);
	}

	@Override
	@Transactional(readOnly=true)
	public Paciente findByCpf(String cpf) {
		return pacienteRepository.findByCpf(cpf);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Paciente> findAll() {
		return pacienteRepository.findAll();
	}
	
	@Override
	public List<Paciente> findByNomeContainingIgnoreCase(String nome) {
		return pacienteRepository.findByNomeContainingIgnoreCase(nome);
	}

	@Override
	public void update(Paciente paciente) {
		pacienteRepository.save(paciente);
	}

	@Override
	public void delete(Long id) {
		Paciente paciente = pacienteRepository.findById(id).get();
		pacienteRepository.delete(paciente);
	}
	
}
