package br.com.sisodonto.service;

import java.util.Date;
import java.util.List;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.model.Horario;

public interface HorarioService {

	Horario save(Horario horario, Long id_dentista);
	Horario findById(Long id);
	List<Horario> findByData(Date data);
	List<Horario> findByDentista(Dentista dentista); // TALVEZ AQUI SEJA UM Long REFERENTE AO ID DO DENTISTA
	List<Horario> findAll();
	void update(Horario horario);
	void delete(Long id);
}
