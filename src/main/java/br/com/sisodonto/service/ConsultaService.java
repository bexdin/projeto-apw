package br.com.sisodonto.service;

import java.util.Date;
import java.util.List;

import br.com.sisodonto.model.Consulta;
import br.com.sisodonto.model.Paciente;

public interface ConsultaService {

	Consulta save(Consulta consulta, Long id_horario);
	Consulta findById(Long id);
	List<Consulta> findByPaciente(Paciente paciente); // TALVEZ AQUI SEJA UM Long REFERENTE AO ID DO PACIENTE
	List<Consulta> findByData(Date data);
	List<Consulta> findByDentista(Long idDentista);
	List<Consulta> findAll();
	void update(Consulta consulta);
	void delete(Long id);

}