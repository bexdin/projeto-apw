package br.com.sisodonto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sisodonto.model.Endereco;
import br.com.sisodonto.repository.EnderecoRepository;

@Service
@Transactional
public class EnderecoServiceImpl implements EnderecoService {
	
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Override
	public Endereco save(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}

	@Override
	@Transactional(readOnly=true)
	public Endereco findById(Long id) {
		return enderecoRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public Endereco findByCep(String cep) {
		return enderecoRepository.findByCep(cep);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Endereco> findByBairro(String bairro) {
		return enderecoRepository.findByBairro(bairro);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Endereco> findByCidade(String cidade) {
		return enderecoRepository.findByCidade(cidade);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Endereco> findByEstado(String estado) {
		return enderecoRepository.findByEstado(estado);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Endereco> findAll() {
		return enderecoRepository.findAll();
	}

	@Override
	public void update(Endereco endereco) {
		enderecoRepository.save(endereco);
	}

	@Override
	public void delete(Long id) {
		Endereco endereco = enderecoRepository.findById(id).get();
		enderecoRepository.delete(endereco);		
	}

}
