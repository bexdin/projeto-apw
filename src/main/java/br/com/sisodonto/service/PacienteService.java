package br.com.sisodonto.service;

import java.util.List;

import br.com.sisodonto.model.Paciente;

public interface PacienteService {

	Paciente save(Paciente paciente);
	Paciente findById(Long id);
	Paciente findByNome(String nome);
	Paciente findByCpf(String cpf);
	List<Paciente> findAll();
	List<Paciente> findByNomeContainingIgnoreCase(String nome);
	void update(Paciente paciente);
	void delete(Long id);
	
}
