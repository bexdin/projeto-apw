package br.com.sisodonto.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.model.Horario;
import br.com.sisodonto.repository.DentistaRepository;
import br.com.sisodonto.repository.HorarioRepository;

@Service
@Transactional
public class HorarioServiceImpl implements HorarioService {
	
	@Autowired
	private HorarioRepository horarioRepository;
	
	@Autowired
	private DentistaRepository dentistaRepository;

	@Override
	public Horario save(Horario horario, Long id_dentista) {
		Dentista dentista = dentistaRepository.findById(id_dentista).get();
		horario.setDentista(dentista);
		return horarioRepository.save(horario);
	}

	@Override
	@Transactional(readOnly=true)
	public Horario findById(Long id) {
		return horarioRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public List<Horario> findByData(Date data) {
		return horarioRepository.findByData(data);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Horario> findByDentista(Dentista dentista) {
		return horarioRepository.findByDentista(dentista);
	}

	@Override
	public List<Horario> findAll() {
		return horarioRepository.findAll() ;
	}

	@Override
	public void update(Horario horario) {
		horarioRepository.save(horario);
	}

	@Override
	public void delete(Long id) {
		Horario horario = horarioRepository.findById(id).get();
		horarioRepository.delete(horario);
	}
	
}
