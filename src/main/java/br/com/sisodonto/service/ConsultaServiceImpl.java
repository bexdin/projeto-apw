package br.com.sisodonto.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sisodonto.model.Consulta;
import br.com.sisodonto.model.Horario;
import br.com.sisodonto.model.Paciente;
import br.com.sisodonto.repository.ConsultaRepository;
import br.com.sisodonto.repository.HorarioRepository;

@Service
@Transactional
public class ConsultaServiceImpl implements ConsultaService {

	@Autowired
	private ConsultaRepository consultaRepository;
	
	@Autowired
	private HorarioRepository horarioRepository;

	@Override
	public Consulta save(Consulta consulta, Long id_horario) {
		Horario horario = horarioRepository.findById(id_horario).get();
		consulta.setHorario(horario);
		return consultaRepository.save(consulta);
	}

	@Override
	@Transactional(readOnly=true)
	public Consulta findById(Long id) {
		return consultaRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public List<Consulta> findByPaciente(Paciente paciente) {
		return consultaRepository.findByPaciente(paciente);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Consulta> findByData(Date data) {
		return consultaRepository.findByData(data);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Consulta> findByDentista(Long idDentista) {
		return consultaRepository.findByDentista(idDentista);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Consulta> findAll() {
		return consultaRepository.findAll();
	}

	@Override
	public void update(Consulta consulta) {
		consultaRepository.save(consulta);
	}

	@Override
	public void delete(Long id) {
		Consulta consulta = consultaRepository.findById(id).get();
		consultaRepository.delete(consulta);
	}
	
}
