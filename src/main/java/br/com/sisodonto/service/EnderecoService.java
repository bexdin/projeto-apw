package br.com.sisodonto.service;

import java.util.List;

import br.com.sisodonto.model.Endereco;

public interface EnderecoService {
	
	Endereco save(Endereco endereco);
	Endereco findById(Long id);
	Endereco findByCep(String cep);
	List<Endereco> findByBairro(String bairro);
	List<Endereco> findByCidade(String cidade);
	List<Endereco> findByEstado(String estado);
	List<Endereco> findAll();
	void update(Endereco endereco);
	void delete(Long id);

}
