package br.com.sisodonto.service;

import java.util.List;

import br.com.sisodonto.model.Dentista;

public interface DentistaService {
	
	Dentista save(Dentista dentista);
	Dentista findById(Long id);
	Dentista findByNome(String nome);
	Dentista findByCro(String cro);
	List<Dentista> findAll();
	void update(Dentista dentista); // TALVEZ AQUI SEJA UM Long REFERENTE AO ID DO DENTISTA
	void delete(Long id);

}
