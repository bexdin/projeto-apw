package br.com.sisodonto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sisodonto.model.Dentista;
import br.com.sisodonto.repository.DentistaRepository;

@Service
@Transactional
public class DentistaServiceImpl implements DentistaService {
	
	@Autowired
	private DentistaRepository dentistaRepository;

	@Override
	public Dentista save(Dentista dentista) {
		return dentistaRepository.save(dentista);
	}

	@Override
	@Transactional(readOnly=true)
	public Dentista findById(Long id) {
		return dentistaRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public Dentista findByNome(String nome) {
		return dentistaRepository.findByNome(nome);
	}

	@Override
	@Transactional(readOnly=true)
	public Dentista findByCro(String cro) {
		return dentistaRepository.findByCro(cro);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Dentista> findAll() {
		return dentistaRepository.findAll();
	}

	@Override
	public void update(Dentista dentista) {
		dentistaRepository.save(dentista);
	}

	@Override
	public void delete(Long id) {
		Dentista dentista = dentistaRepository.findById(id).get();
		dentistaRepository.delete(dentista);
	}

}
