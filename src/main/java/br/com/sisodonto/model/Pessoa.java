package br.com.sisodonto.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="pessoa")
@Inheritance(strategy = InheritanceType.JOINED) //Identifica que a estratégia de herança será JOINED, ou seja, será feita uma junção através de chaves estrangeiras.
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_pessoa")
	private Long id;

	@NotBlank
	private String nome;

	@NotBlank
	@CPF
	private String cpf;
	
	@NotBlank
	private String sexo;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataNascimento;
	
	@NotBlank
	private String telefone1;
	
	private String telefone2;
	
	@Email
	private String email;
		
   
//GETTER AND SETTERS
	
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getTelefone1() {
		return telefone1;
	}
	
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	
	public String getTelefone2() {
		return telefone2;
	}
	
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

// HASHCODE AND EQUALS
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

/* PARA SABER
 * 
 * >>> Existem três formas de implementar herança:
 *  - Tabela Única por Hierarquia de Classes
 *  - Tabela por Subclasse
 *  - Tabela por Classe Concreta
 * Para esta Classe (juntamente com as classes Dentista e Paciente) utilizou-se a estratégia Tabela por Subclasse, onde teremos a 
 * classe pai (Pessoa) e todas suas filhas (Dentista e Paciente) geradas no banco de dados fisicamente, sendo que em todas as classes
 * filhas teremos uma chave estrangeira que apontará para a classe pai.
 * >>> FONTE: https://www.devmedia.com.br/tipos-de-heranca-no-hibernate/28641
 * 
 * >>> ManyToMany Hibernate: Variações Unidirecional e Bidirecional
 * FONTE: https://www.devmedia.com.br/manytomany-hibernate-variacoes-unidirecional-e-bidirecional/29535
 * 
 * >>> A anotação @ManyToMany tem os seguintes atributos:
 * - mappedBy: é o campo que indica o dono do relacionamento. Este atributo só é necessário quando a associação é bidirecional.
 * - targetEntity: é a classe da entidade que é o destino da associação.
 * - cascade: indica o comportamento em cascata da associação, o default é none (nenhum).
 * - fetch: indica o comportamento de busca da associação, sendo que o default é LAZY.
 * >>> FONTE: https://www.devmedia.com.br/hibernate-mapping-mapeando-relacionamentos-entre-entidades/29445
 * 
 * >>> Tipos de cascade que existem e como eles funcionam na modelagem relacional com JPA:
 * NONE = Não faz nada com o objeto (padrão)
 * MERGE = Atualiza filhos quando atualiza o pai, somente se já estiver persisitido
 * PERSIST = Salva o filho quando salva o pai
 * REFRESH = Salva o pai e mantém o filho sem alterar
 * REMOVE = Remove o filho quando remove o pai e vice-versa
 * ALL = Executa todas as operações de cascade
 * DETACH = Realiza a operação detach em cascata
 * >>> FONTE: https://pt.stackoverflow.com/questions/193412/quais-s%C3%A3o-os-tipos-de-cascade-no-jpa
*/
