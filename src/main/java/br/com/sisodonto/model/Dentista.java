package br.com.sisodonto.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="dentista")
@PrimaryKeyJoinColumn(name="id_pessoa") // Identifica que o campo id_pessoa fará a "junção" entre a tabela Dentista e a tabela Pessoa.
public class Dentista extends Pessoa {

	private static final long serialVersionUID = 1L;

	private String cro;
	
	@OneToMany(mappedBy="dentista", targetEntity=Horario.class)
	private List<Horario> horarios;

	
//GETTER AND SETTERS
	
	public String getCro() {
		return cro;
	}

	public void setCro(String cro) {
		this.cro = cro;
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}
	
}
