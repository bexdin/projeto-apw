package br.com.sisodonto.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="horario")
public class Horario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date data;
	
	@NotNull
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:mm")
	private Date horaInicial;
	
	@NotNull
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:mm")
	private Date horaFinal;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_dentista")
	private Dentista dentista;
	
	@OneToOne(mappedBy="horario", optional=true) // TALVEZ NÃO PRECISE DESSE ATRIBUTO!
	private Consulta consulta;
	
// GETTERS AND SETTERS

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(Date horaInicial) {
		this.horaInicial = horaInicial;
	}

	public Date getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(Date horaFinal) {
		this.horaFinal = horaFinal;
	}

	public Dentista getDentista() {
		return dentista;
	}

	public void setDentista(Dentista dentista) {
		this.dentista = dentista;
	}
	
	public Consulta getConsulta() {
		return consulta;
	}
	
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
// HASHCODE AND EQUALS
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horario other = (Horario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

/* PARA SABER
 * 
 * >>> A anotação @OneToOne tem os seguintes atributos (todos opicionais):
 * - targetEntity: é a classe da entidade que é o destino da associação. O default é o tipo do campo ou a propriedade que armazena a associação.
 * - cascade: pode ser configurado para qualquer um dos membros da enumeração javax.persistence.CascadeType.
 * - fetch: pode ser configurado para EAGER ou LAZY.
 * - optional: indica se o valor sendo mapeado pode ser null.
 * - orphanRemoval: indica que se o valor sendo mapeado é deletado, esta entidade também será deletada.
 * - mappedBy: indica que um relacionamento one-to-one bidirecional é apropriado pela entidade nomeada. O dono possui a chave-primária da entidade subordinada.
 * >>> FONTE: https://www.devmedia.com.br/hibernate-mapping-mapeando-relacionamentos-entre-entidades/29445 
 *
 * >>> mappedBy trata sobre como relacionamentos de chave estrangeira entre entidades são salvas, e nada tem haver sobre salvar a própria entidade. 
 * 	Além disso, precisa ser colocado sempre na associação que não é o "dono da relação"!
 * >>> FONTE: https://www.devmedia.com.br/associacoes-com-mappedby-no-hibernate/29425
 *
 * >>> Tipos Date e Time
 * >>> FONTE: https://www.devmedia.com.br/java-persistence-api-tipos-persistentes-no-jpa/30594 
 *
 */
