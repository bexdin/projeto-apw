package br.com.sisodonto.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="paciente")
@PrimaryKeyJoinColumn(name="id_pessoa") // Identifica que o campo id_pessoa fará a "junção" entre a tabela Paciente e a tabela Pessoa.
public class Paciente extends Pessoa{

	private static final long serialVersionUID = 1L;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_endereco")
	private Endereco endereco;
	
	@OneToMany(mappedBy="paciente", targetEntity=Consulta.class)
	private List<Consulta> consultas;

	
//GETTER AND SETTERS	
	
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Consulta> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<Consulta> consultas) {
		this.consultas = consultas;
	}
	
}

