package br.com.sisodonto.security;

public interface SecurityService {
	
    String findLoggedInUsername();
    void autoLogin(String username, String password);
    
}
