package br.com.sisodonto.security;


public interface UserService {
	
	User findByUsername(String username);
	void save(User user);
	
}